# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-29 12:45-0400\n"
"PO-Revision-Date: 2018-09-27 02:31+0000\n"
"Last-Translator: Manuela Silva <inactive+h_manuela_rodsilva@transifex.com>\n"
"Language-Team: Portuguese (http://www.transifex.com/rosarior/mayan-edms/language/pt/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:21 permissions.py:7
msgid "Smart settings"
msgstr ""

#: apps.py:29
msgid "Setting count"
msgstr ""

#: apps.py:33
msgid "Name"
msgstr "Nome"

#: apps.py:37
msgid "Value"
msgstr "Valor"

#: apps.py:40
msgid "Overrided by environment variable?"
msgstr ""

#: apps.py:41
msgid "Yes"
msgstr "Sim"

#: apps.py:41
msgid "No"
msgstr "Não"

#: forms.py:12
msgid "Enter the new setting value."
msgstr ""

#: forms.py:31
msgid "Value must be properly quoted."
msgstr ""

#: forms.py:40
#, python-format
msgid "\"%s\" not a valid entry."
msgstr ""

#: links.py:12 links.py:16
msgid "Settings"
msgstr ""

#: links.py:21
msgid "Namespaces"
msgstr ""

#: links.py:25
msgid "Edit"
msgstr "Editar"

#: permissions.py:10
msgid "Edit settings"
msgstr ""

#: permissions.py:13
msgid "View settings"
msgstr ""

#: views.py:18
msgid "Setting namespaces"
msgstr ""

#: views.py:34
msgid ""
"Settings inherited from an environment variable take precedence and cannot "
"be changed in this view. "
msgstr ""

#: views.py:37
#, python-format
msgid "Settings in namespace: %s"
msgstr ""

#: views.py:45
#, python-format
msgid "Namespace: %s, not found"
msgstr ""

#: views.py:60
msgid "Setting updated successfully."
msgstr ""

#: views.py:68
#, python-format
msgid "Edit setting: %s"
msgstr ""
